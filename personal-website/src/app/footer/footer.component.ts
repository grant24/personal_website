import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  template: `
    <footer class="hero-foot">
      <div class="container content has-text-centered">
        <p>
          Made with love by Greg. Hosted through IPFS.
        </p>
        <a href="https://www.linkedin.com/in/gregory-anto/" target="_blank" class="buttons has-addons is-centered">
          <button class="button is-medium is-info is-light">
            <span class="icon">
              <img src="assets/img/linkedin.png">
            </span>
          </button>
        </a>
        <a href="https://bitbucket.org/grant24/" target="blank" class="buttons has-addons is-centered">
          <button class="button is-medium is-info is-light">
            <span class="icon">
              <img src="assets/img/bitbucket.svg">
            </span>
          </button>
        </a>
      </div>
    </footer>
  `,
  styles: [
  ]
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
