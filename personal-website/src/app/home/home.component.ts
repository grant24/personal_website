import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    <section class="hero is-info is-fullheight-with-navbar">
      <div class="hero-body">
        <section class="section is-large section-padding-large">
          <figure class="image is-square">
            <img class="has-ratio is-rounded" src="/assets/img/portrait_profesional_avi.jpg">
          </figure>
          <h1 class="title">
            Gregory Anto
          </h1>
        </section>
        <section class="section is-large section-padding-large">
          <div class="content is-small>">
            <p style="color:#FFFFFF" > I am an aspiring software engineer with a B.S. in computer
              science from SUNY University at Buffalo.</p>
          </div>
        </section>
      </div>
    </section>
  `,
  styles: [`
    .hero {
      background-image: url('/assets/img/background.jpg') !important;
      background-size: cover;
      background-position: center center;
    }
  `]
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
