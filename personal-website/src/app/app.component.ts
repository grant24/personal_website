import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <!-- header -->
    <app-header></app-header>

    <!-- home section -->
    <app-home></app-home>

    <!-- routes injected here -->
    <router-outlet></router-outlet>

    <!-- footer -->
    <app-footer><
  `,
  styles: []
})
export class AppComponent {
  title = 'personal-website';
}